<?php

//echo dirname( __FILE__ );
 
require_once(dirname( __FILE__ ).'/../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../wp-load.php');
/*
* Plugin Name: dc_bike_tracking
* Description: Desarrollado para que los clientes pueden hacer tracking de las reparaciones de sus bicicletas
* Version: 1.0
* Author: DiegoCardona(diego0123@gmail.com)
* Autor Uri: http://thedeveloper.co
*/

/* helpers */
function get_url(){
	$plugin_dir = plugin_dir_path( __FILE__ );
	return plugin_dir_path( __FILE__ );
}

function styles_scripts() {
    
}
add_action( 'wp_enqueue_scripts', 'styles_scripts', 11 );

// register jquery and style on initialization
add_action('init', 'register_script');
function register_script() {
    //custom
    wp_register_script( 'custom_jquery', plugins_url('/js/custom.js', __FILE__), array('jquery'), '2.5.1' );
    wp_register_script( 'script_download', plugins_url('/js/script.js', __FILE__), array('jquery'), '2.5.1' );
    wp_register_script( 'jcarousel', plugins_url('/js/jcarousel.responsive.js', __FILE__), array('jquery'), '1.5.1' );
    wp_register_script( 'jcarousel_jq', plugins_url('/js/jquery.jcarousel.min.js', __FILE__), array('jquery'), '1.5.1' );

    wp_register_style( 'new_style', plugins_url('/css/style.css', __FILE__), false, '1.0.0', 'all');
    wp_register_style( 'jcarousel_css', plugins_url('/css/jcarousel.responsive.css', __FILE__), false, '1.0.0', 'all');
    

    //bootstrap

    // wp_register_script( 'boostrap_js', plugins_url('/js/bootstrap.min.js', __FILE__), array('jquery'), '2.5.1' );

    // wp_register_style( 'boostrap_style', plugins_url('/css/bootstrap.min.css', __FILE__), false, '1.0.0', 'all');

    // //bootstrap ui
    // wp_enqueue_script('jquery-ui-datepicker');
    // wp_enqueue_style('jquery-style','https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css'); 
}

// use the registered jquery and style above
add_action('wp_enqueue_scripts', 'enqueue_style2');

function enqueue_style2(){
   wp_enqueue_script('custom_jquery');
   wp_enqueue_script('script_download');
   wp_enqueue_script('jcarousel');
   wp_enqueue_script('jcarousel_jq');
   wp_enqueue_style( 'new_style' );
   wp_enqueue_style( 'jcarousel_css' );
   

   // wp_enqueue_script('boostrap_js');
   // wp_enqueue_style( 'boostrap_style' );
}


/*******************/

add_action( 'admin_menu', 'dc_bike_tracking_custom_admin_menu' );
 
function dc_bike_tracking_custom_admin_menu() {
    add_options_page(
        'Tracking - Configuracion',
        'Tracking - Configuracion',
        'manage_options',
        'dc-bike-tracking',
        'dc_bike_tracking_config_page'
    );
}

// function rtb_maps_js_include()
// {
//   wp_enqueue_script('script.js', WP_PLUGIN_URL. '/dc_bike_tracking/js/script.js' );
// }

// add_action( 'admin_enqueue_scripts', 'rtb_maps_js_include' );


function dc_bike_tracking_main_page() {
	define('DC_BIKE_TRACKING_PLUGUIN_NAME','dc_bike_tracking');
	include_once get_url() .'view/tracking.php';    
}

function dc_bike_tracking_config_page() {
    define('DC_BIKE_TRACKING_PLUGUIN_NAME','dc_bike_tracking');
    include_once get_url() .'view/main_page.php';    
}


function create_plugin_database_table()
{
    error_log('creating plugin');
    global $table_prefix, $wpdb;

    $tblname = 'tracking_bikes';
    $wp_track_table = $table_prefix . "$tblname";

    #Check to see if the table exists already, if not, then create it

    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {

        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `serie`  varchar(128)   NOT NULL, ";
        $sql .= "  `marca`  varchar(128)   , ";
        $sql .= "  `modalidad`  varchar(128)   NOT NULL, ";
        $sql .= "  `tam_rin`  varchar(128)   NOT NULL, ";
        $sql .= "  `material`  varchar(128)   NOT NULL, ";
        $sql .= "  `grupo`  varchar(128), ";
        $sql .= "  `frenos`  varchar(128), ";
        $sql .= "  `suspension`  varchar(128)   NOT NULL, ";
        $sql .= "  `status`  varchar(128)   NOT NULL, ";
        $sql .= "  `user_id`  int(11) NOT NULL, ";
        $sql .= "  PRIMARY KEY `order_id` (`id`) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        error_log('creando '.$sql);
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    $tblname = 'tracking_services';
    $wp_track_table = $table_prefix . "$tblname";

    #Check to see if the table exists already, if not, then create it

    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {

        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `fecha`  varchar(128)   NOT NULL, ";
        $sql .= "  `clase_servicio`  varchar(128)   , ";
        $sql .= "  `observaciones`  varchar(2000)   NOT NULL, ";
        $sql .= "  `fotos_id`  varchar(2000)   NOT NULL, ";
        $sql .= "  `fotos_url`  TEXT   NOT NULL, ";
        $sql .= "  `status`  varchar(128)   NOT NULL, ";
        $sql .= "  `bike_id`  int(11) NOT NULL, ";
        $sql .= "  PRIMARY KEY `order_id` (`id`) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        error_log('creando '.$sql);
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    $tblname = 'tracking_fields';
    $wp_track_table = $table_prefix . "$tblname";

    #Check to see if the table exists already, if not, then create it

    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {

        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `name`  varchar(1280)   NOT NULL, ";
        $sql .= "  `key`  varchar(1280)   NOT NULL, ";
        $sql .= "  `value`  varchar(2000)   NOT NULL, ";
        $sql .= "  PRIMARY KEY `order_id` (`id`) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        error_log('creando '.$sql);
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
}

 register_activation_hook( __FILE__, 'create_plugin_database_table' );




function dc_bike_tracking_prepare_init_data(){
    
    include(dirname( __FILE__ ).'/controller/form_front.php');
    $data['WP_PLUGIN_URL'] = WP_PLUGIN_URL;
    $opts = array(
      'http'=>array(
        'method'=>"POST",
        'header'=>"Accept-language: en\r\n" .
                  "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                  "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n",
        'content' => http_build_query($data)
      )
    );

    return stream_context_create($opts);
}

function dc_cotizador_prepare_init_data_v2($url){
    
    include(dirname( __FILE__ ).'/controller/form_front.php');
    $data['WP_PLUGIN_URL'] = WP_PLUGIN_URL;

   $fields_string = http_build_query($data);
    $ch = curl_init($url); 
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    $output = curl_exec ($ch); 
    curl_close ($ch); 
    //error_log($output);
    return $output;
}

function dc_bike_tracking_shortcode() { 
   
   //  $context = dc_bike_tracking_prepare_init_data();
   // return file_get_contents(dirname( __FILE__ ). '/view/tracking.php', false, $context); 
    //return dc_cotizador_prepare_init_data_v2(WP_PLUGIN_URL. '/dc_cotizador/view/tracking.php');
    $res = '


<div class="container-md">
    <div class="row">
        <div class="col dc_tracking_col_table">
            <button type="button" class="btn btn-primary pull-right dc_modal_crear" data-toggle="modal" >Registre su Bicicleta</button>
        </div>
    </div>
    <div class="row">
        <div class="col dc_tracking_col_table">
            <br>
            <div class="alert alert-success dc_tracking_no_results" role="alert" style="display:none">
              No hay bicicletas registradas aún.
            </div>
            <table class="table dc_tracking_si_results" style="display:none">
              <thead>
                <tr>
                  <th scope="col" class="dc_tracking_table_desk_col">#</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Marca</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Modalidad</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Número De Serie</th>
                  <th scope="col" class="dc_tracking_table_mob_col">Marca-Serie</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody class="dc_results">
                              
              </tbody>
            </table>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade dc_modal_bike" id="dcModalBike"  tabindex="-1" aria-labelledby="dcModalBikeLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dc_modal_title" id="dcModalBikeLabel">adf</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Número de serie</label>
                  <input type="text" class="form-control" id="dc_tracking_serie">
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Marca</label>
                  <select id="dc_tracking_marca"></select>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_modalidad">Modalidad</label>
                  <select id="dc_tracking_modalidad"></select>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_rin">Tamaño del Rin</label>
                  <select id="dc_tracking_rin"></select>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_material">Material de fabricación</label>
                  <select id="dc_tracking_material"></select>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_grupo">Grupo de cambios</label>
                  <select id="dc_tracking_grupo"></select>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_frenos">Frenos</label>
                  <select id="dc_tracking_frenos"></select>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_suspension">Suspensión</label>
                  <select id="dc_tracking_suspension"></select>
                </div>
              </div>
              
              
            </form>
          </div>
          <div class="modal-footer">
            <input type="hidden" id="dc_tracking_id" value="">
            <button type="button" class="btn btn-primary dc_tracking_save">Guardar Cambios</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal - View -->
    <div class="modal fade dc_modal_bike_view" id="dcModalBikeView"  tabindex="-1" aria-labelledby="dcModalBikeViewLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dc_modal_title" id="dcModalBikeViewLabel">Mi Bicicleta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link dc_tracking_nav_active dc_tracking_view_nav_bike" href="#">Bicicleta</a>
              </li>
              <li class="nav-item">
                <a class="nav-link dc_tracking_view_nav_service" href="#">Servicios</a>
              </li>
            </ul>
             <form class="dc_tracking_view_bike">
              

              <div class="divTable" style="border: 1px solid #ddd;" >
                  <div class="divTableBody">
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Número de serie</div>
                          <div class="divTableCell dc_cotizador_type">
                            <span id="dc_tracking_serie_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Marca</div>
                          <div class="divTableCell dc_cotizador_from">
                            <span id="dc_tracking_marca_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Modalidad</div>
                          <div class="divTableCell dc_cotizador_to">
                            <span id="dc_tracking_modalidad_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow dc_row_peso_table">
                          <div class="divTableCell divTableCell30">Tamaño del Rin</div>
                          <div class="divTableCell dc_cotizador_kg">
                            <span id="dc_tracking_rin_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Material de fabricación</div>
                          <div class="divTableCell dc_cotizador_precio">
                            <span id="dc_tracking_material_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Grupo de cambios</div>
                          <div class="divTableCell dc_cotizador_seguro">
                            <span id="dc_tracking_grupo_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Frenos</div>
                          <div class="divTableCell dc_cotizador_total">
                            <span id="dc_tracking_frenos_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Suspensión</div>
                          <div class="divTableCell dc_cotizador_total">
                            <span id="dc_tracking_suspension_view"></span>
                          </div>
                      </div>
                  </div>
              </div>

            </form>
             <form class="dc_tracking_view_service">
              <div class="row">
                <div class="col">
                   <div class="alert alert-success dc_tracking_no_results_services" role="alert" style="display:none">
                    No hay servicios registrados.
                  </div>
                  <table class="table dc_tracking_si_results_services" >
                    <thead>
                      <tr>
                        <th scope="col" class="">#</th>
                        <th scope="col" class="">Fecha</th>
                        <th scope="col" class="">Clase de Servicio</th>
                        <th scope="col" class="">Observación</th>
                        <th scope="col" class="">Accion</th>
                      </tr>
                    </thead>
                    <tbody class="dc_results_services">
                                    
                    </tbody>
                  </table>
                </div>
                
              </div>
            </form>

            <form class="dc_tracking_view_service_view" style="display: none">
              <br>
              
              <div class="divTable" style="border: 1px solid #ddd;" >
                  <div class="divTableBody">
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Fecha</div>
                          <div class="divTableCell dc_cotizador_type">
                            <span id="dc_tracking_service_view_fecha"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Clase de servicio</div>
                          <div class="divTableCell dc_cotizador_from">
                            <span id="dc_tracking_service_view_clase"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Observaciones adicionales</div>
                          <div class="divTableCell dc_cotizador_to">
                            <span id="dc_tracking_service_view_observaciones"></span>
                          </div>
                      </div>
                       <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Foto(s)</div>
                          <div class="divTableCell dc_cotizador_to">
                            <button type="button" class="btn btn-primary dc_tracking_slider_fotos" >Ver</button>
                          </div>
                      </div>
                  </div>
              </div>
            
            </form>

            <div class="row dc_tracking_slider" style="display:none;margin-right: 15px;margin-left: 15px;">
                <div class="col">
                    <div class="row">
                      <div class="col">
                        <div class="your-class">
                          <ul class="dc_tracking_slider_elements" >
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="row">  
                      <div class="col" style="text-align:center;">
                        <button type="button" class="btn btn-secondary dc_car_ant"> < Anterior</button>
                        <button type="button" class="btn btn-secondary dc_car_next"> Siguiente > </button>
                      </div>
                    </div>
                </div>
              
            </div> 
            
            

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary dc_tracking_service_back" style="display:none;">Regresar</button>
          </div>
         
        </div>
      </div>
    </div>
    
</div>';
return $res;
} 

add_shortcode('dc-bike-tracking-shortcode', 'dc_bike_tracking_shortcode');

?>