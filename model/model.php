<?php 

require_once(dirname( __FILE__ ).'/../../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../../wp-load.php');

class Tracking{

	public function getService($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if( !($user_id > 0)){
			return false;
		}
		

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_services WHERE id = '%s' ", $id), OBJECT );
			

		return $results;		
	}

	public function getBikes(){
		global $wpdb;
		$user_id = get_current_user_id();

		if( !($user_id > 0)){
			return false;
		}

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes WHERE  `user_id` = '%s' ORDER BY `id` DESC ", $user_id), OBJECT );
		return $results;		
	}

	public function getBike($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes WHERE  `user_id` = '%s' AND `id` = '%s' ORDER BY `id` DESC ", $user_id, $id), OBJECT );
		return $results;		
	}

	public function getBikeServices($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if( !($user_id > 0)){
			return false;
		}
		

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_services WHERE bike_id = '%s' ORDER BY id DESC ", $id), OBJECT );
			

		return $results;		
	}


	public function insert_bike($serie, $marca, $modalidad, $rin, $material, $grupo, $frenos, $suspension){

		
		$messages = [];
		if(empty($serie)){
			$messages[] = "- La serie es obligatoria \n";
		}
		if(empty($marca)){
			$messages[] = "- La marca es obligatoria \n";
		}
		if(empty($modalidad)){
			$messages[] = "- La modalidad es obligatoria \n";
		}
		if(empty($rin)){
			$messages[] = "- El rin es obligatorio \n";
		}
		if(empty($material)){
			$messages[] = "- El material es obligatorio \n";
		}
		if(empty($grupo)){
			$messages[] = "- El grupo es obligatorio \n";
		}
		if(empty($frenos)){
			$messages[] = "- El freno es obligatoria \n";
		}
		if(empty($suspension)){
			$messages[] = "- La suspension es obligatoria \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}


		global $wpdb;
		$result = $wpdb->insert('wp_tracking_bikes', array('serie' => $serie,
													'marca' => $marca,
													'modalidad' => $modalidad,
													'tam_rin' => $rin,
													'material' => $material,
													'grupo' => $grupo,
													'frenos' => $frenos,
													'suspension' => $suspension,
													'user_id' => $user_id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido crear la bicicleta', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'La bicicleta ha sido creada con exito');
	}

	public function update_bike($id, $serie, $marca, $modalidad, $rin, $material, $grupo, $frenos, $suspension){

		
		$messages = [];
		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}
		if(empty($serie)){
			$messages[] = "- La serie es obligatoria \n";
		}
		if(empty($marca)){
			$messages[] = "- La marca es obligatoria \n";
		}
		if(empty($modalidad)){
			$messages[] = "- La modalidad es obligatoria \n";
		}
		if(empty($rin)){
			$messages[] = "- El rin es obligatorio \n";
		}
		if(empty($material)){
			$messages[] = "- El material es obligatorio \n";
		}
		if(empty($grupo)){
			$messages[] = "- El grupo es obligatorio \n";
		}
		if(empty($frenos)){
			$messages[] = "- El freno es obligatoria \n";
		}
		if(empty($suspension)){
			$messages[] = "- La suspension es obligatoria \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}


		global $wpdb;
		$result = $wpdb->update('wp_tracking_bikes', array('serie' => $serie,
													'marca' => $marca,
													'modalidad' => $modalidad,
													'tam_rin' => $rin,
													'material' => $material,
													'grupo' => $grupo,
													'frenos' => $frenos,
													'suspension' => $suspension,
													'user_id' => $user_id),
												array('id' => $id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido actualizar la bicicleta', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'La bicicleta ha sido actulizada con exito');
	}

	public function deleteBike($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}

		$result = $wpdb->delete('wp_tracking_bikes',array('id' => $id, 'user_id' => $user_id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido eliminar la bicicleta', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'La bicicleta ha sido eliminada con exito');	
	}

	public function get_all_parameters(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT * FROM wp_tracking_fields ', OBJECT );
		//$mylink = $result = $wpdb->query('SELECT * FROM wp_rtb_place ');
		return $results;		
	}

	public function insert_parameter($parameter, $reg){
		
		global $wpdb;
		$result = $wpdb->insert('wp_tracking_fields', array('name' => $parameter['name'], 'key'=>$parameter['key'], 'value'=>$parameter['value']));
		if(!$result){
			return array('status' => 'ER', 'message'=>'no se ha podido crear el parametro', 'key'=>$parameter['key'], 'reg' => $reg);
		}
		return array('status' => 'OK');
	}

	public function delete_all_paramteres(){
		global $wpdb;
		$delete = $wpdb->query("TRUNCATE TABLE `wp_tracking_fields`");
	}

	public function getParameter($key){
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT `value` FROM wp_tracking_fields 
					WHERE  `key` = '%s' ", $key ), OBJECT );			
		
		if(count($results) == 0){
			$results = array("status" => 'ER', 'message' => 'no se pudo recueperar el parametro' );
		}
		return $results[0]->value;
	}

}

?>