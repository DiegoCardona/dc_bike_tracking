<?php 

function exportar_excel($parameters){

	
	$objPHPExcel = new PHPExcel();
	   
   //Informacion del excel
   $objPHPExcel->
    getProperties()
        ->setCreator("thedeveloper.co")
        ->setLastModifiedBy("thedeveloper.co")
        ->setTitle("DC Tracking")
        ->setSubject("DC Tracking")
        ->setDescription("DC Tracking")
        ->setKeywords("DC Tracking")
        ->setCategory("DC Tracking");    
	   
	   $sheet = 1;
	   $i = 1;  

	   $objWorkSheet = $objPHPExcel->createSheet($sheet);
	   $objPHPExcel->setActiveSheetIndex($sheet)->setTitle("Parametros"); 
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, 'NOMBRE');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, 'CLAVE');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, 'VALOR');
	 

	   foreach($parameters as $parameter){
	      $i++; 
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, $parameter->name);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, $parameter->key);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, $parameter->value);
	      
	   }
	 	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		//$objWriter->save('php://output');
		$file = md5(date("YmdHis")).'.xlsx';
	 	$objWriter->save('../temp/'. $file);
	 	return $file;
}

function cargar_desde_excel(&$parameters, $archivo){

	$objPHPExcel = PHPExcel_IOFactory::load($archivo);
	//Obtengo el numero de filas del archivo

	$objPHPExcel->setActiveSheetIndex(0);
	//Obtengo el numero de filas del archivo
	$numRows = $objPHPExcel->setActiveSheetIndex(1)->getHighestRow();
	$informacion = array();
	for ($i = 2; $i <= $numRows; $i++) {
	    $parameters[] = array(
	        'name' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
	        'key' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue(),
	        'value' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue()
	    );
	 }
}

?>