<?php 

header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');


// require_once '../lib/excel/PHPExcel.php';
// require_once("../lib/excel/PHPExcel/IOFactory.php");

require_once '../model/model.php';
// require_once '../controller/exportar.php';

if(!isset($_POST['action']) && !isset($_GET['action'])){
	$resultado = array('status'=>'ERROR', 'message' =>'ha ocurrido un error');
	//echo json_encode($resultado);
	exit();
}

if(isset($_POST['action'])){
	$action = $_POST['action'];	
}else{
	$action = $_GET['action'];
}



switch ($action) {
	case 'getBikes':
			
			$tracking = new Tracking();
			$bikes = $tracking->getBikes();
			$parameters = $tracking->get_all_parameters();

			$parametersList = [];
			foreach ($parameters as $parameter) {
				$parametersList[$parameter->key] = $parameter->value;
			}

			$result = array('status' => 'OK','bikes' => $bikes, 'parameters' => $parametersList, 'plugin_url' => WP_PLUGIN_URL);
			echo json_encode($result);
		break;

	case 'getBike':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$bike = $tracking->getBike($id);
			
			$result = array('status' => 'OK','bike' => $bike[0]);
			echo json_encode($result);
		break;

	case 'getBikeAndServices':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$bike = $tracking->getBike($id);
			$services = $tracking->getBikeServices($id);
			
			$result = array('status' => 'OK','bike' => $bike[0],'services' => $services, 'plugin_url' => WP_PLUGIN_URL);
			echo json_encode($result);
		break;

		case 'deleteBike':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$result = $tracking->deleteBike($id);			
			echo json_encode($result);
		break;

	case 'newBike':
			
			$id 				= $_POST['id'];
			$serie 				= $_POST['serie'];
			$marca				= $_POST['marca'];
			$modalidad			= $_POST['modalidad'];
			$rin 				= $_POST['rin'];
			$material 			= $_POST['material'];
			$grupo 				= $_POST['grupo'];
			$frenos 			= $_POST['frenos'];
			$suspension 		= $_POST['suspension'];
			$tracking = new Tracking();
			if($id == null || $id == ''){
				$result = $tracking->insert_bike($serie, $marca, $modalidad
									, $rin, $material, $grupo, $frenos, $suspension);	
			}else{
				$result = $tracking->update_bike($id, $serie, $marca, $modalidad
									, $rin, $material, $grupo, $frenos, $suspension);
			}
			

			echo json_encode($result);
		break;
	
	case 'getService':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$service = $tracking->getService($id);
			
			$result = array('status' => 'OK','service' => $service[0], 'plugin_url' => WP_PLUGIN_URL);
			echo json_encode($result);
		break;
		
	default:
		# code...
		break;
}


?>