<div class="container-md">
    <div class="row">
        <div class="col dc_tracking_col_table">
            <button type="button" class="btn btn-primary pull-right dc_modal_crear" data-toggle="modal" >Registre su Bicicleta</button>
        </div>
    </div>
    <div class="row">
        <div class="col dc_tracking_col_table">
            <br>
            <div class="alert alert-success dc_tracking_no_results" role="alert" style="display:none">
              No hay bicicletas registradas aún.
            </div>
            <table class="table dc_tracking_si_results" style="display:none">
              <thead>
                <tr>
                  <th scope="col" class="dc_tracking_table_desk_col">#</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Marca</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Modalidad</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Número De Serie</th>
                  <th scope="col" class="dc_tracking_table_mob_col">Marca-Serie</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody class="dc_results">
                              
              </tbody>
            </table>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade dc_modal_bike" id="dcModalBike"  tabindex="-1" aria-labelledby="dcModalBikeLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dc_modal_title" id="dcModalBikeLabel">adf</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Número de serie</label>
                  <input type="text" class="form-control" id="dc_tracking_serie">
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Marca</label>
                  <select id="dc_tracking_marca"></select>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_modalidad">Modalidad</label>
                  <select id="dc_tracking_modalidad"></select>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_rin">Tamaño del Rin</label>
                  <select id="dc_tracking_rin"></select>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_material">Material de fabricación</label>
                  <select id="dc_tracking_material"></select>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_grupo">Grupo de cambios</label>
                  <select id="dc_tracking_grupo"></select>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_frenos">Frenos</label>
                  <select id="dc_tracking_frenos"></select>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_suspension">Suspensión</label>
                  <select id="dc_tracking_suspension"></select>
                </div>
              </div>
              
              
            </form>
          </div>
          <div class="modal-footer">
            <input type="hidden" id="dc_tracking_id" value="">
            <button type="button" class="btn btn-primary dc_tracking_save">Guardar Cambios</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal - View -->
    <div class="modal fade dc_modal_bike_view" id="dcModalBikeView"  tabindex="-1" aria-labelledby="dcModalBikeViewLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dc_modal_title" id="dcModalBikeViewLabel">Mi Bicicleta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link dc_tracking_nav_active dc_tracking_view_nav_bike" href="#">Mi Bicicleta</a>
              </li>
              <li class="nav-item">
                <a class="nav-link dc_tracking_view_nav_service" href="#">Mis Servicios</a>
              </li>
            </ul>
             <form class="dc_tracking_view_bike">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Número de serie</label>
                  <span id="dc_tracking_serie_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Marca</label>
                  <span id="dc_tracking_marca_view"></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Modalidad</label>
                  <span id="dc_tracking_modalidad_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Tamaño del Rin</label>
                  <span id="dc_tracking_rin_view"></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Material de fabricación</label>
                  <span id="dc_tracking_material_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Grupo de cambios</label>
                  <span id="dc_tracking_grupo_view"></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Frenos</label>
                  <span id="dc_tracking_frenos_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Suspensión</label>
                  <span id="dc_tracking_suspension_view"></span>
                </div>
              </div>
            </form>
            <form class="dc_tracking_view_service">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Número de serie</label>
                  <span id="dc_tracking_serie_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Marca</label>
                  <span id="dc_tracking_marca_view"></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Modalidad</label>
                  <span id="dc_tracking_modalidad_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Tamaño del Rin</label>
                  <span id="dc_tracking_rin_view"></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Material de fabricación</label>
                  <span id="dc_tracking_material_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Grupo de cambios</label>
                  <span id="dc_tracking_grupo_view"></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="dc_tracking_serie">Frenos</label>
                  <span id="dc_tracking_frenos_view"></span>
                </div>
                <div class="form-group col-md-6">
                  <label for="dc_tracking_marca">Suspensión</label>
                  <span id="dc_tracking_suspension_view"></span>
                </div>
              </div>
            </form>
            

          </div>
         
        </div>
      </div>
    </div>
    
</div>