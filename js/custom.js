var dc_car_current_element = 0;


jQuery(document).ready(function(){
	jQuery('.dc_modal_crear').on('click', function(){
		jQuery('.dc_modal_title').text("Registrar Bicicleta");
		jQuery('#dc_tracking_id').val("");
		jQuery('#dc_tracking_serie').val("");
		jQuery('#dc_tracking_marca').val("");
		jQuery('#dc_tracking_modalidad').val("");
		jQuery('#dc_tracking_rin').val("");
		jQuery('#dc_tracking_material').val("");
		jQuery('#dc_tracking_grupo').val("");
		jQuery('#dc_tracking_frenos').val("");
		jQuery('#dc_tracking_suspension').val("");
		jQuery('.dc_modal_bike').modal('show')	
	})	


	jQuery('.dc_tracking_save').on('click', function(){
		registrarBicileta()	
	})	

	jQuery('.dc_tracking_view_nav_bike').on('click', function(){
		jQuery('.dc_tracking_view_nav_service').removeClass('dc_tracking_nav_active');
		jQuery('.dc_tracking_view_nav_bike').addClass('dc_tracking_nav_active');
		jQuery('.dc_tracking_view_bike').css('display', '');
		jQuery('.dc_tracking_view_service').css('display', 'none');
		jQuery('.dc_tracking_view_service_view').css('display', 'none');
		jQuery(".dc_tracking_service_back").css('display', 'none');
		jQuery(".dc_tracking_slider").css('display', 'none');
	})	
	jQuery('.dc_tracking_view_nav_service').on('click', function(){
		jQuery('.dc_tracking_view_nav_bike').removeClass('dc_tracking_nav_active');
		jQuery('.dc_tracking_view_nav_service').addClass('dc_tracking_nav_active');
		jQuery('.dc_tracking_view_bike').css('display', 'none');
		jQuery('.dc_tracking_view_service_view').css('display', 'none');
		jQuery('.dc_tracking_view_service').css('display', '');
		jQuery(".dc_tracking_service_back").css('display', 'none');
		jQuery(".dc_tracking_slider").css('display', 'none');
		

	})	

	jQuery('.dc_tracking_service_back').on('click', function(){
		jQuery('.dc_tracking_view_nav_service').trigger('click');
	});

	jQuery('.dc_tracking_slider_fotos').on('click', function(){
		dc_car_current_element = 0;
		jQuery('.dc_tracking_slider').css('display', '')
		jQuery('.dc_tracking_view_service_view').css('display', 'none');
		jQuery('.dc_car_next').trigger('click');
	});	


	jQuery('.dc_car_ant').on('click', function(){
		console.log('click')
		dc_car_ant();
	});	

	jQuery('.dc_car_next').on('click', function(){
		console.log('click')
		dc_car_next();
	});	

	jQuery('.dc_carrusel_li').css('width','500px !important');
	
	loadData();

});


var loadData = function(){

	var postData = {action: 'getBikes'};
	var _url = "/wp-content/plugins/dc_bike_tracking/controller/api.php";
	
	jQuery.ajax({
	        type: "POST",
	        dataType: "json",
	        url: _url,
	        data: postData,
	        success: function(data){
	        	if(data.status == 'OK'){
	        		loadDataAfter(data);
	        	}else{
	        		jQuery('.dc_tracking_si_results').css('display', 'none');
					jQuery('.dc_tracking_no_results').css('display', '');
	        	}
	        },
	        error: function(e){
	            console.log(e.message);
	        }
	});

	jQuery('.dc_tracking_no_results').css('display', 'none');
	jQuery('.dc_tracking_si_results').css('display', '');

}

var dc_set_select_values = function(parameter, id){
	parameter = parameter.split("|");
	var select = "<option value=''>Seleccione una opción</option>"
	for (var i = 0; i < parameter.length; i++) {
		select += "<option value='"+parameter[i]+"'>"+parameter[i]+"</option>";
	}
	jQuery('#'+id).html(select);
}

var loadDataAfter = function(data){

	var parameters = data.parameters;

	dc_set_select_values(parameters['MARCA'], 'dc_tracking_marca');
	dc_set_select_values(parameters['MODALIDAD'], 'dc_tracking_modalidad');
	dc_set_select_values(parameters['RIN_SIZE'], 'dc_tracking_rin');
	dc_set_select_values(parameters['MATERIAL'], 'dc_tracking_material');
	dc_set_select_values(parameters['GRUPO'], 'dc_tracking_grupo');
	dc_set_select_values(parameters['FRENOS'], 'dc_tracking_frenos');
	dc_set_select_values(parameters['SUSPENSION'], 'dc_tracking_suspension');

	var bikes = data.bikes;
	var records = ""
	for (var i = 0; i < bikes.length; i++) {
		records += "<tr>";
		records += "<td class='dc_tracking_table_desk_col'>"+(i+1)+"</td>";
		records += "<td class='dc_tracking_table_desk_col'>"+bikes[i].marca+"</td>";
		records += "<td class='dc_tracking_table_desk_col'>"+bikes[i].modalidad+"</td>";
		records += "<td class='dc_tracking_table_desk_col'>"+bikes[i].serie+"</td>";
		records += "<td class='dc_tracking_table_mob_col'>"+bikes[i].marca+'-'+bikes[i].serie+"</td>";
		records += "<td>";
		records += "<a class='dc_tracking_view' data-id='"+bikes[i].id+"'><img class='dc_tracking_img_icon' src='"+data.plugin_url+"/dc_bike_tracking/css/search.png'/></a>";
		records += "&nbsp;&nbsp;&nbsp;";
		records += "<a class='dc_tracking_edit' data-id='"+bikes[i].id+"'><img class='dc_tracking_img_icon' src='"+data.plugin_url+"/dc_bike_tracking/css/edit.png'/></a>";
		records += "&nbsp;&nbsp;&nbsp;";
		records += "<a class='dc_tracking_delete' data-id='"+bikes[i].id+"'><img class='dc_tracking_img_icon' src='"+data.plugin_url+"/dc_bike_tracking/css/delete.png'/></a>";
		records += "</td>";
		records += "</tr>";
	}
	jQuery('.dc_results').html(records);

	/*Events for buttons*/
	jQuery('.dc_tracking_view').on('click', function(){

		loadBike(jQuery(this).data('id'));
	})
	jQuery('.dc_tracking_edit').on('click', function(){
		loadBikeToEdit(jQuery(this).data('id'));
	})
	jQuery('.dc_tracking_delete').on('click', function(){
		deleteBike(jQuery(this).data('id'))
	})

	if(data.bikes.length > 0){
		jQuery('.dc_tracking_no_results').css('display', 'none');
		jQuery('.dc_tracking_si_results').css('display', '');
		jQuery('.dc_modal_crear').css('display', '');
	}else{
		jQuery('.dc_tracking_si_results').css('display', 'none');
		jQuery('.dc_tracking_no_results').css('display', '');
		jQuery('.dc_modal_crear').css('display', '');
	}

	

}

/********************** CREATE ******************************/

var registrarBicileta = function(){


	var postData = {action: 'newBike'};
	postData['id'] 					= jQuery('#dc_tracking_id').val();
	postData['serie'] 				= jQuery('#dc_tracking_serie').val();
	postData['marca'] 				= jQuery('#dc_tracking_marca').val();
	postData['modalidad'] 			= jQuery('#dc_tracking_modalidad').val();
	postData['rin'] 				= jQuery('#dc_tracking_rin').val();
	postData['material'] 			= jQuery('#dc_tracking_material').val();
	postData['grupo'] 				= jQuery('#dc_tracking_grupo').val();
	postData['frenos'] 				= jQuery('#dc_tracking_frenos').val();
	postData['suspension'] 			= jQuery('#dc_tracking_suspension').val();
	
	var _url = "/wp-content/plugins/dc_bike_tracking/controller/api.php";
	
	jQuery.ajax({
	        type: "POST",
	        dataType: "json",
	        url: _url,
	        data: postData,
	        success: function(data){
	        	if(data.status == 'OK'){
	        		registrarBiciletaAfter(data);
	        	}else{
	        		if(data.hasOwnProperty('messages')){
	        			var messages = data.messages.join("");
	        			alert(messages);
					}
					if(data.hasOwnProperty('message')){
	        			alert(data.message);
					}
	        		
	        	}
	        },
	        error: function(e){
	            console.log(e.message);
	        }
	});

	jQuery('.dc_tracking_no_results').css('display', 'none');
	jQuery('.dc_tracking_si_results').css('display', '');

}

var registrarBiciletaAfter = function(data){
	alert(data.message);
	jQuery('.dc_modal_bike').modal('hide');
	loadData();
}

/************************* EDITAR ******************************/

var loadBikeToEdit = function(id){
		
		var postData = {action: 'getBike', id: id};
		var _url = "/wp-content/plugins/dc_bike_tracking/controller/api.php";
		
		jQuery.ajax({
		        type: "POST",
		        dataType: "json",
		        url: _url,
		        data: postData,
		        success: function(data){
		        	if(data.status == 'OK'){
		        		loadBikeToEditAfter(data);
		        	}else{
		        		if(data.hasOwnProperty('messages')){
		        			var messages = data.messages.join("");
		        			alert(messages);
						}
						if(data.hasOwnProperty('message')){
		        			alert(data.message);
						}
		        		
		        	}
		        },
		        error: function(e){
		            console.log(e.message);
		        }
		});		
}

var loadBikeToEditAfter = function(data){

	var bike = data.bike;

	jQuery('#dc_tracking_id').val(bike.id);
	jQuery('#dc_tracking_serie').val(bike.serie);
	jQuery('#dc_tracking_marca').val(bike.marca);
	jQuery('#dc_tracking_modalidad').val(bike.modalidad);
	jQuery('#dc_tracking_rin').val(bike.tam_rin);
	jQuery('#dc_tracking_material').val(bike.material);
	jQuery('#dc_tracking_grupo').val(bike.grupo);
	jQuery('#dc_tracking_frenos').val(bike.frenos);
	jQuery('#dc_tracking_suspension').val(bike.suspension);


	jQuery('.dc_modal_title').text("Editar Bicicleta");
	jQuery('.dc_modal_bike').modal('show')	
}


/************************* VIEW ******************************/

var loadBike = function(id){

	var postData = {action: 'getBikeAndServices', id: id};
		var _url = "/wp-content/plugins/dc_bike_tracking/controller/api.php";
		
		jQuery.ajax({
		        type: "POST",
		        dataType: "json",
		        url: _url,
		        data: postData,
		        success: function(data){
		        	if(data.status == 'OK'){
		        		loadBikeAfter(data);
		        	}else{
		        		if(data.hasOwnProperty('messages')){
		        			var messages = data.messages.join("");
		        			alert(messages);
						}
						if(data.hasOwnProperty('message')){
		        			alert(data.message);
						}
		        		
		        	}
		        },
		        error: function(e){
		            console.log(e.message);
		        }
		});	
}

var loadBikeAfter = function(data){
	var bike = data.bike;
	jQuery('#dc_tracking_serie_view').html(bike.serie);
	jQuery('#dc_tracking_marca_view').html(bike.marca);
	jQuery('#dc_tracking_modalidad_view').html(bike.modalidad);
	jQuery('#dc_tracking_rin_view').html(bike.tam_rin);
	jQuery('#dc_tracking_material_view').html(bike.material);
	jQuery('#dc_tracking_grupo_view').html(bike.grupo);
	jQuery('#dc_tracking_frenos_view').html(bike.frenos);
	jQuery('#dc_tracking_suspension_view').html(bike.suspension);

	var services = data.services;
	if(services.length > 0){
		var records = ""
		for (var i = 0; i < services.length; i++) {
			records += "<tr>";
			records += "<td class=''>"+services[i].id+"</td>";
			records += "<td class=''>"+services[i].fecha+"</td>";
			records += "<td class=''>"+services[i].clase_servicio+"</td>";
			records += "<td class=''>"+services[i].observaciones.substring(0,20)+"...</td>";
			records += "<td>";
			records += "<a class='dc_tracking_service_view' data-id='"+services[i].id+"'><img class='dc_tracking_img_icon' src='"+data.plugin_url+"/dc_bike_tracking_back/css/search.png'/></a>";
			records += "</td>";
			records += "</tr>";
		}

		jQuery('.dc_results_services').html(records);
		jQuery('.dc_tracking_si_results_services').css('display', '');
		jQuery('.dc_tracking_no_results_services').css('display', 'none');

		jQuery('.dc_tracking_service_view').on('click', function(){
			jQuery('.dc_tracking_view_service_view').css('display', 'none');
			loadService(jQuery(this).data('id'))			
		})
	}else{
		jQuery('.dc_tracking_si_results_services').css('display', 'none');
		jQuery('.dc_tracking_no_results_services').css('display', '');
	}

	
	jQuery('.dc_tracking_view_service').css('display', 'none');
	jQuery('.dc_modal_bike_view').modal('show')	
	
}

var loadService = function(id){
		var postData = {action: 'getService', id: id};
		var _url = "/wp-content/plugins/dc_bike_tracking_back/controller/api.php";
		
		jQuery.ajax({
		        type: "POST",
		        dataType: "json",
		        url: _url,
		        data: postData,
		        success: function(data){
		        	if(data.status == 'OK'){
		        		loadServiceAfter(data);
		        	}else{
		        		if(data.hasOwnProperty('messages')){
		        			var messages = data.messages.join("");
		        			alert(messages);
						}
						if(data.hasOwnProperty('message')){
		        			alert(data.message);
						}
		        		
		        	}
		        },
		        error: function(e){
		            console.log(e.message);
		        }
		});	
}

var loadServiceAfter = function(data){

	var service = data.service;
	
	jQuery('#dc_tracking_service_view_fecha').html(service.fecha);
	jQuery('#dc_tracking_service_view_clase').html(service.clase_servicio);
	jQuery('#dc_tracking_service_view_observaciones').html(service.observaciones);

	var fotos_urls = [];
	if(service.fotos_url.indexOf('|') > 0){
	 	fotos_urls = service.fotos_url.split('|');
	}else{
		fotos_urls.push(service.fotos_url);
	}


	var slider = "";
	for (var i = 0; i < fotos_urls.length; i++) {
		slider += '<li class="dc_carrusel_li" style="display: none;list-style: none;">';
	    slider +='   <img src="'+fotos_urls[i]+'" alt="Image 1" >';
	    slider +=' </li>';
	}


	
    jQuery(".dc_tracking_slider_elements").html(slider);
    
    jQuery(".dc_tracking_view_service_view").css('display', '');
    jQuery(".dc_tracking_service_back").css('display', '');
    jQuery(".dc_tracking_view_service").css('display', 'none');



}

/*************** DELETE ***************/
var deleteBike = function(id){
	if(confirm('¿Está seguro que desea eliminar esta bicicleta de su registro?')){
		var postData = {action: 'deleteBike', id: id};
		var _url = "/wp-content/plugins/dc_bike_tracking/controller/api.php";
		
		jQuery.ajax({
		        type: "POST",
		        dataType: "json",
		        url: _url,
		        data: postData,
		        success: function(data){
		        	if(data.status == 'OK'){
		        		if(data.hasOwnProperty('message')){
		        			alert(data.message);
						}
						loadData();
		        	}else{
		        		if(data.hasOwnProperty('messages')){
		        			var messages = data.messages.join("");
		        			alert(messages);
						}
						if(data.hasOwnProperty('message')){
		        			alert(data.message);
						}
		        		
		        	}
		        },
		        error: function(e){
		            console.log(e.message);
		        }
		});	
	}
}



/**********************  carrusel  ********************************/



var dc_car_ant = function(){
	dc_car_current_element--;
	if(dc_car_current_element < 1){
		dc_car_current_element = 1;
	}
	jQuery('.dc_carrusel_li').css('display', 'none');
	var i = 1;
	jQuery('.dc_tracking_slider_elements').find('li').each(function(){
		if(i == dc_car_current_element){
			jQuery(this).css('display','');
		}
		i++;
	});
}

var dc_car_next = function(){
	dc_car_current_element++;
	if(jQuery('.dc_tracking_slider_elements').find('li').length < dc_car_current_element){
		dc_car_current_element = jQuery('.dc_tracking_slider_elements').find('li').length;
	}
	jQuery('.dc_carrusel_li').css('display', 'none');
	var i = 1;
	jQuery('.dc_tracking_slider_elements').find('li').each(function(){
		if(i == dc_car_current_element){
			jQuery(this).css('display','');
		}
		i++;
	});
}


/*******************************************************************/
/*******************************************************************/
/****************** MANEJO Y CARGA DE ARCHIVOS *********************/
/*******************************************************************/
/*******************************************************************/


jQuery(document).ready(function(){

	jQuery('#dc_tracking_descargar').on('click', function(){
		dc_tracking_descargar();
	});

	jQuery('#dc_tracking_cargar').on('click', function(){
		console.log('descargando');
		dc_tracking_cargar();
	});

	jQuery('.loadResultClose').on('click', function(){
		var modal = document.getElementById("loadResult");
		modal.style.display = "none";
		window.location.reload();
	});

});

/***************************** PROCESAMIENTO DE ARCHIVO **********************************/

var dc_tracking_descargar = function(){
	downloadAjax();
}

var dc_tracking_cargar = function(){
	if(confirm('¿esta seguro de cargar la nueva informacion?')){
		uploadAjax(document.getElementById('dc_tracking_archivo'));
	}
}

/*************************** PROCESAMIENTO DE CAMPANNA ********************************/

var dc_tracking_get_all_campanna = function(){
	
}

var dc_tracking_get_campanna = function(){

}

var dc_tracking_edit_campanna = function(){
	
}

var dc_tracking_save_campanna = function(){
	
}

/******* GENERIC ********************/


var callback_error = function(data){
	alert('error '+JSON.stringify(data));
}

var callback_success_default = function(data){
	alert('error '+JSON.stringify(data));
}

var ajax_post = function(postData, callback, callback_error){
	var _url = controller;
	show_loading();
	jQuery.ajax({
	        type: "POST",
	        dataType: "json",
	        url: _url,
	        data: postData,
	        success: function(data){
	        	if(data.status == 'OK'){
	        		window[callback](data);	
	        	}else{
	        		window[callback_error](data.message);
	        	}
	        	hide_loading();
	        },
	        error: function(e){
	        	hide_loading();
	            console.log(e.message);
	            callback_error(e.message);
	        }
	});
}

var show_loading = function(){
	//jQuery('#rtb_loading').css('display','');
}

var hide_loading = function(){
	//jQuery('#rtb_loading').css('display','none');	
}


var uploadAjax = function(inputFile){
	//console.log('antes');
	var file = inputFile.files[0];
	var data = new FormData();
	data.append('archivo',file);
	data.append('action', jQuery('#dc_tracking_action').val());
	//console.log('durante');
	var _url = jQuery('#dc_tracking_plugin_path').val() + '/dc_bike_tracking/controller/controller.php';
	//console.log(_url);
	jQuery.ajax({
		url:_url,
		type:'POST',
		contentType:false,
		data:data,
		processData:false,
		cache:false,
		success: function(data){
        	if(data.status == 'OK'){
        		var modal = document.getElementById("loadResult");
        		modal.style.display = "block";
        		console.log(data.log_cotization)
        		console.log(data.log_cotization.length)
        		if(data.log_cotization.length == 0 && data.log_parameters.length == 0){
        			jQuery('.modalMessage').html("con exito");
        		}else{
        			jQuery('.modalMessage').html("con errores");
        			if(data.log_cotization.length != 0){
        				var table = "<table border=1 style='border-collapse: collapse;'>";
        				table += "<tr><td><b>fila</b></td><td><b>Origen</b></td><td><b>Destino</b></td><td><b>Mensaje</b></td>";
        				for (var i = 0; i < data.log_cotization.length; i++) {
        					table += "<tr><td>"+data.log_cotization[i].reg+"</td>";
        					table += "<td>"+data.log_cotization[i].from+"</td>";
        					table += "<td>"+data.log_cotization[i].to+"</td>";
        					table += "<td>"+data.log_cotization[i].message+"</td>";
        					table += "</tr>";
        				}
        				table += "</table>";
        				jQuery('.modalCotization').html(table);
        			}
        			if(data.log_parameters.length == 0){
        				
        			}
        		}
        		//alert(data.message);
        	}else{
        		alert(data.message);
        	}
        	//alert(JSON.stringify(data));
        },
        error: function(e){
        	
        }
	});

}

var downloadAjax = function(){
	var data = new FormData();
	data.append('action', 'descargando');
	var _url = jQuery('#dc_tracking_plugin_path').val() + '/dc_bike_tracking/controller/controller.php';
	jQuery.ajax({
		url:_url,
		type:'POST',
		contentType:false,
		data:data,
		processData:false,
		cache:false,
		success: function(data){
        	//alert(JSON.stringify(data));
        	if(data.status == 'OK'){
        		//window.open(jQuery('#dc_tracking_plugin_path').val() + '/dc_tracking/temp/'+data.archivo, '', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
        		window.location.href = jQuery('#dc_tracking_plugin_path').val() + '/dc_bike_tracking/temp/'+data.archivo;

        	}else{
        		alert('ha ocurrido un problema descargando el archivo');
        	}
        },
        error: function(e){
        	
        }
	});

}




